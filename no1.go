package main

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

var ID int = 0
var data = [][4]string{{"ID | ", "Judul | ", "Penyanyi | ", "Jumlah Vote"}}

type lagu struct {
	judul    string
	penyanyi string
	vote     float64
}

func add() {
	var lg = struct {
		lagu
		grade int
	}{}

	fmt.Print("Masukan Judul Lagu : ")
	fmt.Scan(&lg.judul)
	fmt.Print("Masukan Nama Penyanyi : ")
	fmt.Scan(&lg.penyanyi)
	ID++
	fmt.Print("Masukan Jumlah Vote : ")
	fmt.Scan(&lg.vote)

	newdata := [4]string{(strconv.Itoa(ID)), lg.judul, lg.penyanyi, (strconv.FormatFloat(lg.vote, 'f', 1, 64))}
	data = append(data, newdata)
	newdata = [4]string{}
}
func showAll() {
	fmt.Print("Daftar judul yang Tersedia:", len(data)-1)
	fmt.Println("")

	for i := 0; i < len(data); i++ {
		for j := 0; j < 4; j++ {
			fmt.Print(data[i][j], "  ")
		}
		fmt.Println("")
	}
}
func showDelete() {
	fmt.Print("Daftar judul yang Tersedia Sekarang :")
	fmt.Println("")

	for i := 0; i < len(data); i++ {
		for j := 0; j < 4; j++ {
			fmt.Print(data[i][j], "  ")
		}
		fmt.Println("")
	}
}
func searchID(ID int) int {
	var index int
	search := strconv.Itoa(ID)
	for i := 0; i < len(data); i++ {
		if data[i][0] == search {
			index = i
			break
		}
	}
	return index
}

func deleteID(ID int) {
	var before, after [][4]string
	index := searchID(ID)
	before = data[:index]
	after = data[index+1:]
	data = [][4]string{}
	for i := 0; i < len(before); i++ {
		data = append(data, before[i])
	}
	for i := 0; i < len(after); i++ {
		data = append(data, after[i])
	}
}

func stringtoFloat(input string) float64 {
	var save float64
	if s, err := strconv.ParseFloat(input, 64); err == nil {
		save = s
	}
	return save
}
func validate(input string) (bool, error) {
	if input == "" {
		return false, errors.New("cannot be empty")
	}
	m := regexp.MustCompile("[a-zA-Z]")
	if m.MatchString(input) == false {
		return false, errors.New("please input strings")
	}
	return true, nil
}

func countVote() float64 {
	var count float64 = 0
	for i := 0; i < len(data); i++ {
		count = count + stringtoFloat(data[i][3])
		fmt.Println(count)
	}
	return count
}

func searchSinger() {
	fmt.Println("Search (a/A): ")
	for i := 0; i < len(data); i++ {
		matched, _ := regexp.MatchString(`^a|^A`, data[i][2])
		if matched == true {
			fmt.Println("ID : ", data[i][0])
			fmt.Println("Judul : ", data[i][1])
			fmt.Println("Penyanyi : ", data[i][2])
			fmt.Println("Jumlah Vote : ", data[i][3])
		}
	}
}

func topthree(){
    if len(data) < 4 {
      fmt.Println("Maaf Data Lagu Sangat Sedikit, Tolong Tambahkan Lagi")
    } else {
    var third,first,second [4]string
    for i:=1;i<len(data);i++{
      if stringtoFloat(data[i][3]) > stringtoFloat(first[3]) {
        third = second
        second = first
        first = data[i]
      } else if stringtoFloat(data[i][3]) > stringtoFloat(second[3]) {
        third = second
        second = data[i]
      } else if stringtoFloat(data[i][3]) > stringtoFloat(third[3]) {
        third = data[i]
      }
    }
    fmt.Print("Top 3 Lagu berdasarkan Voting Terbanyak : \n")
    fmt.Print(data[0][0], "\t", data[0][1], "\t", data[0][2], "\t", data[0][3], "\n")
    fmt.Print(first[0], "\t", first[1], "\t", first[2], "\t", first[3], "\n")
    fmt.Print(second[0], "\t", second[1], "\t", second[2], "\t", second[3], "\n")
    fmt.Print(third[0], "\t", third[1], "\t", third[2], "\t", third[3], "\n")
    }
}

func back() {
	var kembali string

	fmt.Println("========================")
	fmt.Print("Kembali ke menu awal? [Y/N]")
	fmt.Scan(&kembali)
	if kembali == "Y" {
		main()
	} else if kembali == "N" {
		fmt.Println("Thanks")
		os.Exit(1)
	} else {
		fmt.Print("Anda salah Input !!!")
	}
}

func main() {
	var numMenu int

	fmt.Println("========================")
	fmt.Println("Daftar Pilihan :")
	fmt.Println("1. Input data judul baru")
	fmt.Println("2. Hapus data judul berdasarkan id_judul")
	fmt.Println("3. Tampilkan keseluruhan data")
	fmt.Println("4. Tampilkan data dan menghitung Vote")
	fmt.Println("5. Tampilkan TOP 3 Fav")
	fmt.Println("6. Tampilkan keseluruhan data penyanyi berawalan A")
	fmt.Println("0. Keluar")
	fmt.Println("========================")
	fmt.Print("Pilih Menu angka diatas [0...6] : ")
	fmt.Scan(&numMenu)
	if numMenu == 1 {
		add()
		back()

	} else if numMenu == 2 {
		var saver string
		fmt.Print("judul mana yang ingin dihapus [ID]? : ")
		fmt.Scan(&saver)
		var num, err = strconv.Atoi(saver)
		if err == nil && num > 0 {
			deleteID(num)
		}
		showDelete()
		back()
	} else if numMenu == 3 {
		showAll()
		back()
	} else if numMenu == 4 {
		fmt.Println("Sum of all counted voutes : ", countVote())
		back()	
	} else if numMenu == 5 {
    topthree()
    back()
  	}else if numMenu == 6 {
		searchSinger()
		back()
	} else if numMenu == 7 {
		fmt.Println("Thanks")
		os.Exit(1)
	}

}

/*
2.Memasukan data judul, penyanyi dan jumlah vote kedalam list terurut menurut jumlah vote.
Untuk id dibuat langsung secara otomatis oleh sistem dengan syarat id tidak boleh kosong ataupun
sama dengan data lain.
3. Menghapus data menurut ID.
*/
